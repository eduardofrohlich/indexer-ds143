#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define MAX_WORDS 10000
#define MAX_FILES 10

// Estrutura para armazenar informações sobre uma palavra
typedef struct {
    char palavra[100];
    int quantidade;
} Palavra;

// Estrutura para armazenar informações sobre um arquivo
typedef struct {
    char nome[100];
    double relevancia;
} Arquivo;

// Função para comparar duas palavras pelo número de ocorrências (para qsort)
int compareArquivos(const void* a, const void* b) {
    double relevanciaA = ((Arquivo*)a)->relevancia;
    double relevanciaB = ((Arquivo*)b)->relevancia;
    if (relevanciaA > relevanciaB) return -1;
    else if (relevanciaA < relevanciaB) return 1;
    else return 0;
}

int comparePalavras(const void* a, const void* b) {
    return ((Palavra*)b)->quantidade - ((Palavra*)a)->quantidade;
}

// Função para calcular o TF-IDF de uma palavra em um arquivo
double calcularTFIDF(char* palavra, Palavra* palavras, int num_palavras, int total_palavras) {
    int tf = 0;
    for (int i = 0; i < num_palavras; i++) {
        if (strcmp(palavra, palavras[i].palavra) == 0) {
            tf = palavras[i].quantidade;
            break;
        }
    }
    double idf = log((double)total_palavras / (double)tf);
    return tf * idf;
}

// Função para processar uma palavra (converte para minúsculo, remove caracteres não-alfabéticos e divide palavras compostas)
void processarPalavra(char* palavra, Palavra* palavras, int* num_palavras_unicas) {
    char* token = strtok(palavra, " "); // Dividir a entrada em palavras usando espaço como delimitador

    while (token != NULL) {
        // Converte a palavra para minúsculas
        for (int i = 0; token[i]; i++) {
            token[i] = tolower(token[i]);
        }

        // Remove caracteres não-alfabéticos
        int j = 0;
        for (int i = 0; token[i]; i++) {
            if (isalpha(token[i])) {
                token[j++] = token[i];
            }
        }
        token[j] = '\0';

        // Verifica se a palavra tem pelo menos 2 caracteres
        if (strlen(token) >= 2) {
            // Verificar se a palavra já está no array de palavras
            int encontrada = 0;
            for (int i = 0; i < *num_palavras_unicas; i++) {
                if (strcmp(token, palavras[i].palavra) == 0) {
                    palavras[i].quantidade++;
                    encontrada = 1;
                    break;
                }
            }

            // Se a palavra não foi encontrada, adicioná-la ao array
            if (!encontrada) {
                strcpy(palavras[*num_palavras_unicas].palavra, token);
                palavras[*num_palavras_unicas].quantidade = 1;
                (*num_palavras_unicas)++;
            }
        }

        token = strtok(NULL, " "); // Obter a próxima palavra
    }
}

int main(int argc, char* argv[]) {
    // Verifica o número de argumentos
    if (argc < 4) {
        printf("Uso: %s  --freq N ARQUIVO\n\t\t--freq-word PALAVRA ARQUIVO\n\t\t--search TERMO ARQUIVO [ARQUIVO ...]\n", argv[0]);
        return 1;
    }

    // Verifica se a opção é --freq
    if (strcmp(argv[1], "--freq") == 0) {
        int num_palavras = atoi(argv[2]);
        char* filename = argv[3];

        // Ler o arquivo
        FILE* file = fopen(filename, "r");
        if (file == NULL) {
            printf("Erro ao abrir o arquivo.\n");
            return 1;
        }

        // Inicializar um array de palavras
        Palavra palavras[MAX_WORDS];
        int num_palavras_unicas = 0;

        // Ler palavras do arquivo e contar suas ocorrências
        char buffer[100];
        while (fscanf(file, "%s", buffer) != EOF) {
            // Processar a palavra
            processarPalavra(buffer, palavras, &num_palavras_unicas);
        }

        fclose(file);

        // Ordenar o array de palavras pelo número de ocorrências em ordem decrescente
        qsort(palavras, num_palavras_unicas, sizeof(Palavra), comparePalavras);

        // Exibir as N palavras mais frequentes
        for (int i = 0; i < num_palavras && i < num_palavras_unicas; i++) {
            printf("%s - %d\n", palavras[i].palavra, palavras[i].quantidade);
        }
    }
    // Verifica se a opção é --freq-word (palavra específica)
   else if (strcmp(argv[1], "--freq-word") == 0) {
        char* word = argv[2];
        char* filename = argv[3];

        // Ler o arquivo
        FILE* file = fopen(filename, "r");
        if (file == NULL) {
            printf("Erro ao abrir o arquivo.\n");
            return 1;
        }

        // Processar a palavra
        processarPalavra(word, NULL, NULL);

        // Inicializar contador
        int quantidade = 0;

        // Ler palavras do arquivo e contar ocorrências da palavra específica
        char buffer[100];
        while (fscanf(file, "%s", buffer) != EOF) {
            // Processar a palavra
            processarPalavra(buffer, NULL, NULL);
            if (strcmp(buffer, word) == 0) {
                quantidade++;
            }
        }

        fclose(file);

        // Exibir a quantidade de ocorrências da palavra específica
        printf("%s - %d\n", word, quantidade);
    }
    // Verifica se a opção é --search (busca por termo)
    else if (strcmp(argv[1], "--search") == 0) {
        char* termo = argv[2];

        // Inicializar um array de arquivos
        Arquivo arquivos[MAX_FILES];

        // Inicializar contador de arquivos
        int num_arquivos = 0;

        // Loop para processar todos os arquivos fornecidos como argumentos
        for (int i = 3; i < argc; i++) {
            char* filename = argv[i];

            // Ler o arquivo
            FILE* file = fopen(filename, "r");
            if (file == NULL) {
                printf("Erro ao abrir o arquivo %s.\n", filename);
                return 1;
            }

            // Inicializar um array de palavras
            Palavra palavras[MAX_WORDS];
            int num_palavras_unicas = 0;

            // Ler palavras do arquivo e contar suas ocorrências
            char buffer[100];
            while (fscanf(file, "%s", buffer) != EOF) {
                // Processar a palavra
                processarPalavra(buffer, palavras, &num_palavras_unicas);
            }

            fclose(file);

            // Calcular a relevância do termo no arquivo
            double relevancia = 0.0;
            for (int j = 0; j < num_palavras_unicas; j++) {
                double tfidf = calcularTFIDF(palavras[j].palavra, palavras, num_palavras_unicas, num_palavras_unicas);
                if (strstr(termo, palavras[j].palavra) != NULL) {
                    relevancia += tfidf;
                }
            }

            // Armazenar o nome do arquivo e sua relevância
            strcpy(arquivos[num_arquivos].nome, filename);
            arquivos[num_arquivos].relevancia = relevancia;
            num_arquivos++;
        }

        // Ordenar o array de arquivos por relevância em ordem decrescente
        qsort(arquivos, num_arquivos, sizeof(Arquivo), compareArquivos);

        // Exibir a lista de arquivos mais relevantes
        for (int i = 0; i < num_arquivos; i++) {
            printf("%s - %.2lf\n", arquivos[i].nome, arquivos[i].relevancia);
        }
    }
    // Opção inválida
    else {
        printf("Opção inválida: %s\n", argv[1]);
        return 1;
    }

    return 0;
}
